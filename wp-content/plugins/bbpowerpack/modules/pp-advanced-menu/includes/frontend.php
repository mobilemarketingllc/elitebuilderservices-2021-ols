<?php
$schema_attrs = apply_filters( 'pp_advanced_menu_nav_render_schema_attrs', true, $settings ) ? ' itemscope="itemscope" itemtype="https://schema.org/SiteNavigationElement"' : '';
if ( $settings->mobile_breakpoint == 'always' ) {
	include $module->dir . 'includes/menu-' . $settings->mobile_menu_type . '.php';
} else {
	include $module->dir . 'includes/menu-default.php';
	if ( 'default' != $settings->mobile_menu_type ) {
		include $module->dir . 'includes/menu-' . $settings->mobile_menu_type . '.php';
	}
}
