<?php

// Defines
define( 'FL_CHILD_THEME_DIR', get_stylesheet_directory() );
define( 'FL_CHILD_THEME_URL', get_stylesheet_directory_uri() );

remove_action('wp_head', 'wp_generator');
// Classes
require_once 'classes/class-fl-child-theme.php';

// Actions
add_action( 'wp_enqueue_scripts', 'FLChildTheme::enqueue_scripts', 1000 );

add_action( 'wp_enqueue_scripts', function(){
    wp_enqueue_script("slick",get_stylesheet_directory_uri()."/resources/slick/slick.min.js","","",1);
    wp_enqueue_script("cookie",get_stylesheet_directory_uri()."/resources/jquery.cookie.js","","",1);
   // wp_enqueue_script("child-script",get_stylesheet_directory_uri()."/script.js","","",1);
});


// Register menus
function register_my_menus() {
    register_nav_menus(
        array(
            'footer-1' => __( 'Footer Menu 1' ),
            'footer-2' => __( 'Footer Menu 2' ),
            'footer-3' => __( 'Footer Menu 3' ),
            'footer-4' => __( 'Footer Menu 4' ),
            'footer-5' => __( 'Footer Menu 5' ),
            'site-map' => __( 'Site Map' ),
        )
    );
}
add_action( 'init', 'register_my_menus' );

// Enable shortcodes in text widgets
add_filter('widget_text','do_shortcode');

// Move Yoast to bottom
function yoasttobottom()
{
    return 'low';
}

add_filter('wpseo_metabox_prio', 'yoasttobottom');


//Facet Title Hook
add_filter( 'facetwp_shortcode_html', function( $output, $atts ) {
    if ( isset( $atts['facet'] ) ) {       
        $output= '<div class="facet-wrap"><strong>'.$atts['title'].'</strong>'. $output .'</div>';
    }
    return $output;
}, 10, 2 );


add_filter( 'body_class', 'codismo_body_class' );
function codismo_body_class( $classes ) {
 
    global $post;
    if ( 'carpeting' === $post->post_type ) {

      		$classes[] = 'carpeting';
	}
 
	return $classes;
 
}

// gallery fun
add_action('wp_ajax_myfilter', 'gallery_filter_function'); // wp_ajax_{ACTION HERE} 
add_action('wp_ajax_nopriv_myfilter', 'gallery_filter_function');
 
function gallery_filter_function(){

    // for taxonomies / categories
    $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
    $args = array(
        'post_type' => 'gallery',
        'posts_per_page' => 9,
        'paged' => $paged
    );
	if($_POST['roomfilter'] !="" ){
		$args['tax_query'] = array(
			array(
				'taxonomy' => 'room',
				'field' => 'id',
				'terms' => $_POST['roomfilter']
			)
		);
    }
    if( $_POST['stylesfilter'] !="" ){
		$args['tax_query'] = array(
			array(
				'taxonomy' => 'styles',
				'field' => 'id',
				'terms' => $_POST['stylesfilter']
			)
		);
    }
    if( $_POST['roomfilter'] !="" && $_POST['stylesfilter'] !="" ){
		$args['tax_query'] = array(
            'relation' => 'AND',
			array(
				'taxonomy' => 'room',
				'field' => 'id',
				'terms' => $_POST['roomfilter']
            ),
            array(
				'taxonomy' => 'styles',
				'field' => 'id',
				'terms' => $_POST['stylesfilter']
			)
		);
    }

	$custom_query = new WP_Query( $args );
 ?>
		<?php if($custom_query->have_posts()) { ?>
					<div class="gallery_list_holder row">
					<?php
						$popup_id = 0;
						while($custom_query->have_posts()) :
							$custom_query->the_post();
							
							if(has_post_thumbnail()){
								?>
								<div class="col-md-4">
									<div class="gallery-item">
										<div class="gallery-image" >
											<a href="javascript:void(0)" data-toggle="modal" data-target="#gallery-modal-<?php echo $popup_id; ?>" >
												<?php //echo get_the_post_thumbnail_url(get_the_ID(), 'full'); ?>
												<div class="fearured_img_holder">
													<?php the_post_thumbnail('full'); ?>
													<span class="fa fa-search-plus" aria-hidden="true"></span>
												</div>
												<!-- <h5 id="gallery-modal-label-<?php echo $popup_id; ?>"><a href="<?php the_permalink(); ?>" ><?php the_title(); ?></a></h5> -->
											</a>
										</div>
										<div class="modal fade" id="gallery-modal-<?php echo $popup_id; ?>"  role="dialog" >
											<div class="modal-dialog" role="document">
												<div class="modal-content">
													<div class="modal-header">
														<button type="button" class="close" data-dismiss="modal">&times;</button>
													</div>
													<div class="modal-body">
														<div class="popup-holder">
															<?php
																$images = get_field('gallery_images');
																$size = 'full'; // (thumbnail, medium, large, full or custom size)
															?>
															<!-- slider start here -->

															<div class="carouselWrapper">
																<div class="carouselOne owl-carousel">
																	<?php   if( $images ):
																			foreach( $images as $image_id ):
																				if($image_id["url"] != "") {
																				if($image_id["alt"]) { $img_alt = $image_id["alt"]; } else { $img_alt = $image_id["title"]; }
																	?>
																			<div class="item" data-hash="<?php echo $img_alt; ?>">
																					<img src="<?php echo $image_id["url"]; ?>" alt="<?php echo $img_alt; ?>" />
																				<?php } ?>
																			</div>
																		<?php
																			endforeach;
																			endif;
																		?>
																</div> 
															</div> 

															<div class="sliderThubnail">	
																<h5 style="padding-left: 20px!important; margin: 0px!important;">More</h5>			
																<div class="carouselTwo  owl-carousel">
																	<?php   if( $images ):
																			foreach( $images as $image_id ):
																	?>
																			<div class="item">
																				<?php
																					if($image_id["url"] != "") {
																					if($image_id["alt"]) { $img_alt = $image_id["alt"]; } else { $img_alt = $image_id["title"]; }
																				?>
																					<a class="button secondary url" href="#<?php echo $img_alt; ?>">
																						<img src="<?php echo $image_id["url"]; ?>" alt="<?php echo $img_alt; ?>" />
																					</a>
																				<?php } ?>
																			</div>
																		<?php
																			endforeach;
																			endif;
																		?>
																</div> 				
															</div>				
															<!-- slider start end -->
														</div>
													</div>
													<div class="modal-footer">
														<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
													</div>						
												</div>
											</div>
										</div>
									</div>
								</div>								
								<?php
									$popup_id++;
							}		
							$gallery_imgs = get_field('gallery_images');
							if($gallery_imgs){
								foreach($gallery_imgs as $gallery_img){
									if($gallery_img["alt"]) { $img_alt = $gallery_img["alt"]; } else { $img_alt = $gallery_img["title"]; }
								?>
									<div class="col-md-4">
										<div class="gallery-item">
											<div class="gallery-image" >
												<a href="javascript:void(0)" data-toggle="modal" data-target="#gallery-modal-<?php echo $popup_id; ?>" >
													<?php //echo get_the_post_thumbnail_url(get_the_ID(), 'full'); ?>
													<div class="fearured_img_holder">
													<img width="2400" height="1600" src="<?php echo $gallery_img['url'];?>" class="attachment-full size-full wp-post-image" alt="<?php echo $img_alt;?>" loading="lazy" >
														<span class="fa fa-search-plus" aria-hidden="true"></span>
													</div>
													<!-- <h5 id="gallery-modal-label-<?php echo $popup_id; ?>"><a href="<?php the_permalink(); ?>" ><?php the_title(); ?></a></h5> -->
												</a>
											</div>
											<div class="modal fade" id="gallery-modal-<?php echo $popup_id; ?>"  role="dialog" >
												<div class="modal-dialog" role="document">
													<div class="modal-content">
														<div class="modal-header">
															<button type="button" class="close" data-dismiss="modal">&times;</button>
														</div>
														<div class="modal-body">
															<div class="popup-holder">
																<?php
																	?>
																<!-- slider start here -->

																<div class="carouselWrapper">
																	<div class="carouselOne owl-carousel">
																		<div class="item" data-hash="<?php echo $img_alt; ?>">
																			<img src="<?php echo $gallery_img["url"]; ?>" alt="<?php echo $img_alt; ?>" />
																		</div>
																	</div> 
																</div> 

																<div class="sliderThubnail">	
																	<h5 style="padding-left: 20px!important; margin: 0px!important;">More</h5>			
																	<div class="carouselTwo  owl-carousel">
																		<div class="item">
																			<a class="button secondary url" href="#<?php echo $img_alt; ?>">
																				<img src="<?php echo $gallery_img["url"]; ?>" alt="<?php echo $img_alt; ?>" />
																			</a>
																		</div>
																	</div> 				
																</div>				
																<!-- slider start end -->
															</div>
														</div>
														<div class="modal-footer">
															<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
														</div>						
													</div>
												</div>
											</div>
										</div>
									</div>								
								<?php
									$popup_id++;
								}
							}
						endwhile;
					?>
					</div>
					<div class="col-md-12 text-center">
						<div class="pagination">
							<?php 
								echo paginate_links( array(
									'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
									'total'        => $custom_query->max_num_pages,
									'current'      => max( 1, get_query_var( 'paged' ) ),
									'format'       => '?paged=%#%',
									'show_all'     => false,
									'type'         => 'plain',
									'end_size'     => 2,
									'mid_size'     => 1,
									'prev_next'    => true,
									'prev_text'    => sprintf( '<i></i> %1$s', __( 'Newer Posts', 'text-domain' ) ),
									'next_text'    => sprintf( '%1$s <i></i>', __( 'Older Posts', 'text-domain' ) ),
									'add_args'     => false,
									'add_fragment' => '',
								) );
							?>
						</div>
					</div>
					<?php } else { ?>
						<div class="no-gallery-found">no results found - todo: add this message from admin</div>
					<?php }  ?>
					</div>
					
				</div><?php

 
	die();
}

function room_menu_items_function() {
    if( $terms = get_terms( array( 'taxonomy' => 'room', 'orderby' => 'name' ,'hide_empty' => false))  ) : 
 
        $return_string = '<ul>';
        foreach ( $terms as $term ) :
            $return_string .= '<li class="tax_li" data-img="'.sanitize_title($term->name).'"><a href="/galleries/?room='.$term->term_id.'">' . $term->name . '</a></li>'; // ID of the category as the value of an option
        endforeach;
        $return_string .='</ul>';
    endif;
    wp_reset_query();
    return $return_string;
 }
 add_shortcode('ROOM-MENU-ITEMS', 'room_menu_items_function');

 
function styles_menu_items_function() {
    if( $terms = get_terms( array( 'taxonomy' => 'styles', 'orderby' => 'name' ,'hide_empty' => false))  ) : 
 
        $return_string = '<ul>';
        foreach ( $terms as $term ) :
            $return_string .= '<li class="tax_li" data-img="'.sanitize_title($term->name).'"><a href="/galleries/?styles='.$term->term_id.'">' . $term->name . '</a></li>'; // ID of the category as the value of an option
        endforeach;
        $return_string .='</ul>';
    endif;
    wp_reset_query();
    return $return_string;
 }
 add_shortcode('STYLES-MENU-ITEMS', 'styles_menu_items_function');

 
function menu_image_items_function() {
   
    $return_string = '<div class="">';
    $terms = get_terms( array( 'taxonomy' => 'styles', 'orderby' => 'name' ,'hide_empty' => false));
    $img_url ="/wp-content/uploads/2020/06/tlr6-highlands-east-ridgecrest_family-room-300x200-1.jpg";
    $return_string .= '<div class=" tax_img show_first"> <img width="300" src="'.$img_url.'"><p class="term_title">Gellery</p></div>'; 
    if( $terms) : 
        foreach ( $terms as $term ) :
            $img_url = get_field('tax_image' , $term->term_id) ? get_field('tax_image' , $term->term_id): "/wp-content/uploads/2020/06/tlr6-highlands-east-ridgecrest_family-room-300x200-1.jpg";
            $return_string .= '<div class=" tax_img '.sanitize_title($term->name).'"> <img width="300" src="'.$img_url.'"><p class="term_title">' . $term->name . '</p></div>'; // ID of the category as the value of an option
        endforeach;
    endif;

    $terms = get_terms( array( 'taxonomy' => 'room', 'orderby' => 'name' ,'hide_empty' => false));
    if( $terms) : 
        foreach ( $terms as $term ) :
            $img_url = get_field('tax_image' , $term->term_id) ? get_field('tax_image' , $term->term_id): "/wp-content/uploads/2020/06/tlr6-highlands-east-ridgecrest_family-room-300x200-1.jpg";
            $return_string .= '<div class=" tax_img '.sanitize_title($term->name).'"> <img width="300" src="'.$img_url.'"><p class="term_title">' . $term->name . '</p></div>'; // ID of the category as the value of an option
        endforeach;
    endif;
    $return_string .='</div>';
    wp_reset_query();
    return $return_string;
 }
 add_shortcode('MENU-IMAGE-ITEMS', 'menu_image_items_function');

 function filter_site_upload_size_limit( $size ) {
    return 1024 * 1024 * 256;
}
add_filter( 'upload_size_limit', 'filter_site_upload_size_limit', 20 );

//add method to register event to WordPress init

add_action( 'init', 'register_daily_mysql_bin_log_event');
 
function register_daily_mysql_bin_log_event() {
    // make sure this event is not scheduled
    if( !wp_next_scheduled( 'mysql_bin_log_job' ) ) {
        // schedule an event
        wp_schedule_event( time(), 'daily', 'mysql_bin_log_job' );
    }
}

add_action( 'mysql_bin_log_job', 'mysql_bin_log_job_function' );
 

function mysql_bin_log_job_function() {
   
    global $wpdb;
    $yesterday = date('Y-m-d',strtotime("-1 days"));
    $sql_delete = "PURGE BINARY LOGS BEFORE '$yesterday'" ;						
	$delete_endpoint = $wpdb->get_results($sql_delete);
    write_log($sql_delete);	
}